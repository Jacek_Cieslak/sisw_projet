#include <stdlib.h>

#include "jellyRecognizer.h"

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if (argc != 3) {
		std::cout << "Invalid number of arguments (pathToPics pathToOutTxt)" << std::endl;
		return -1;
	}

	jellyRecognizer jelly(argv[1], argv[2]);

	int imgSize = 480;
	jelly.loadAndResizeFiles(imgSize);
	jelly.detectJellysAllImgs();
	jelly.saveRestults();
	return(0);

}
