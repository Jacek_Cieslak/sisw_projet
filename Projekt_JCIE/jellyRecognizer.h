#pragma once
#include <stdlib.h>
#include <string>
#include <vector>
#include <filesystem>
#include <tuple>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

#include "colorPresets.h"
#include "types.h"

namespace fs = std::filesystem;

#define TEST_PICTURES 0
#define DISPLAY 0
#define VERBOSE 0

class jellyRecognizer
{
private:
	std::string inputPath;
	std::string outputPath;
	std::vector<cv::Mat> inputPictures;

	detectionParamsSet prepareDetectionParamsSet(color colorToDetect);

	std::tuple<cv::Mat, int> detectCircles(cv::Mat scrNotBlurred, std::vector<circleMaskParam> cirlesDetectionParams, bool positiveMask = true);
	std::tuple<cv::Mat, int> makeCircleMaskOneRound(cv::Mat scrNotBlurred, circleMaskParam params, bool positiveMask = true);
	std::tuple<cv::Mat, int> makeCircleMaskOneRound(cv::Mat scrNotBlurred, circleMaskParam params, std::vector<cv::Vec3f>& circles, bool positiveMask = true);

	std::tuple<cv::Mat, int, int> detectShapes(cv::Mat srcMasked, std::vector<blobMaskParam> bearDetectionParams, bool positiveMask = true);
	std::tuple<cv::Mat, int, int> detectShapesOneRound(cv::Mat srcMasked, blobMaskParam param, bool positiveMask = true);
	std::tuple<cv::Mat, int, int> detectShapesRectangles(cv::Mat srcMaskedThresholded, limits cutSize, limits cutRatio, limits cutPerc);

	cv::Mat detectJellysOneImg(cv::Mat src, color colorToDetect, oneImgResult& result);

public:
	std::vector<std::string> fileNames;
	std::vector<oneImgResult> results;

	jellyRecognizer();
	jellyRecognizer(std::string inputPath, std::string outputPath);
	~jellyRecognizer();

	uint8_t loadAndResizeFiles(uint16_t smallerRes);
	void saveRestults();

	std::vector<oneImgResult> detectJellysAllImgs();
};

