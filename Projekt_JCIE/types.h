#pragma once
#include <opencv2/opencv.hpp>
#include <stdlib.h>

enum color {
	lightRed,
	darkRed,
	orange,
	green,
	yellow,
	white,
	unknown
};

const std::vector<std::string> colorNames = { "lightRed",
	"darkRed",
	"orange",
	"green",
	"yellow",
	"white",
	"black" 
};

struct limits {
	int low;
	int high;
};

struct basicParam {
	color paramsColor;
	int medianBlur;
	cv::Scalar colorHigh;
	cv::Scalar colorLow;
};

struct circleMaskParam : basicParam {
	int errosionKernelSize;
	int closingKernelSize;
	cv::MorphShapes closingShape;
	int gaussianBlur;
	int houghCircleParams[6];

	circleMaskParam() {
		closingShape = cv::MORPH_RECT;
	}
};

struct blobMaskParam : basicParam {
	int closingKernelSize;
	limits cutSize;
	limits cutRatio;
	limits cutPerc;
};

struct detectionParamsSet {
	std::vector<circleMaskParam> cirlesDetection;
	std::vector<blobMaskParam> bearDetection;
	std::vector<blobMaskParam> snakeDetection;
};

struct oneImgResult {
	int lightRedBear;
	int darkRedBear;
	int orangeBear;
	int greenBear;
	int yellowBear;
	int whiteBear;
	int lightRedCircle;
	int darkRedCircle;
	int orangeCircle;
	int greenCircle;
	int yellowCircle;
	int whiteCircle;
	int greenWhiteSnake;
	int orangeBlackSnake;
	int yellowRedSnake;

	oneImgResult() {
		lightRedBear = 0;
		darkRedBear = 0;
		orangeBear = 0;
		greenBear = 0;
		yellowBear = 0;
		whiteBear = 0;
		lightRedCircle = 0;
		darkRedCircle = 0;
		orangeCircle = 0;
		greenCircle = 0;
		yellowCircle = 0;
		whiteCircle = 0;
		greenWhiteSnake = 0;
		orangeBlackSnake = 0;
		yellowRedSnake = 0;
	}
};