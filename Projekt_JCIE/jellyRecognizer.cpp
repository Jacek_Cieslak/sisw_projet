#include "jellyRecognizer.h"

jellyRecognizer::jellyRecognizer() {
	this->inputPath = "";
	this->outputPath = "JCIE_output.txt";
}
jellyRecognizer::jellyRecognizer(std::string in, std::string out)
{
	this->inputPath = in;
	this->outputPath = out;
}
jellyRecognizer::~jellyRecognizer()
{
}

uint8_t jellyRecognizer::loadAndResizeFiles(uint16_t biggerRes)
{
	uint8_t filesCounter = 0;
	for (const auto & entry : fs::directory_iterator(this->inputPath)) {
		++filesCounter;
	}
	std::cout << std::to_string(filesCounter) << " files found. Starting loading" << std::endl;
	filesCounter = 0;
	for (const auto & entry : fs::directory_iterator(this->inputPath))
	{
		cv::Mat Temp = cv::imread(entry.path().u8string());
		fileNames.push_back(entry.path().filename().string());
		if (Temp.empty()) { printf(entry.path().u8string().data()); system("pause"); return -1; }
		if (Temp.size().height > Temp.size().width) {
			cv::resize(Temp, Temp, cv::Size(biggerRes, (Temp.size().height * biggerRes) / Temp.size().width));

		}
		else {
			cv::resize(Temp, Temp, cv::Size((Temp.size().width * biggerRes) / Temp.size().height, biggerRes));
		}
		this->inputPictures.push_back(Temp);
		++filesCounter;
		std::cout << std::to_string(filesCounter) << "\t" << entry.path() << " " << Temp.size().width << "x" << Temp.size().height << std::endl;
	}
	return filesCounter;
}
void jellyRecognizer::saveRestults()
{
	std::ofstream outputFile;
	outputFile.open(this->outputPath, std::ios::trunc);
	for (int i = 0; i < results.size(); i++) {

		int n1 = results[i].lightRedBear;
		int n2 = results[i].darkRedBear;
		int n3 = results[i].orangeBear;
		int n4 = results[i].greenBear;
		int n5 = results[i].yellowBear;
		int n6 = results[i].whiteBear;
		int n7 = results[i].lightRedCircle;
		int n8 = results[i].darkRedCircle;
		int n9 = results[i].orangeCircle;
		int n10 = results[i].greenCircle;
		int n11 = results[i].yellowCircle;
		int n12 = results[i].whiteCircle;
		int n13 = results[i].greenWhiteSnake;
		int n14 = results[i].orangeBlackSnake;
		int n15 = results[i].yellowRedSnake;

		outputFile << fileNames[i] << " "
			<< n1 << " " << n2 << " " << n3 << " " << n4 << " " << n5 << " " << n6 << " " << n7
			<< " " << n8 << " " << n9 << " " << n10 << " " << n11 << " " << n12 << " " << n13
			<< " " << n14 << " " << n15 << "\n";
		outputFile.flush();
	}
	outputFile.close();
}

cv::Mat jellyRecognizer::detectJellysOneImg(cv::Mat src, color colorToDetect, oneImgResult& result)
{
	std::tuple<cv::Mat, int> circlesTuple;
	std::tuple<cv::Mat, int, int>  bearTuple, snakeTuple;
	detectionParamsSet params;
	params = prepareDetectionParamsSet(colorToDetect);
	cv::Mat maskCircles, srcMaskedCircles;

	circlesTuple = detectCircles(src, params.cirlesDetection, false);


	maskCircles = std::get<0>(circlesTuple);
	src.copyTo(srcMaskedCircles, maskCircles);

	cv::Mat drawingBears, srcMaskedBears;
	bearTuple = detectShapes(srcMaskedCircles, params.bearDetection, false);

	cv::Mat maskBears = std::get<0>(bearTuple);
	src.copyTo(srcMaskedBears, maskBears);

	cv::Mat totalMask;
	maskCircles.copyTo(totalMask, maskBears);
	cv::Mat srcMaskedCirclesBears;
	src.copyTo(srcMaskedCirclesBears, totalMask);

	cv::Mat drawingSnakes, srcMaskedSnakes;
	snakeTuple = detectShapes(srcMaskedCirclesBears, params.snakeDetection, false);
	snakeTuple;

	int bearResult = std::get<1>(bearTuple);
	int snakeResult = std::get<1>(snakeTuple);
	int circleResult = std::get<1>(circlesTuple);

	if (std::get<2>(bearTuple) != 0) {
		circleResult += std::get<2>(bearTuple);
	}

	switch (colorToDetect) {
	case color::white: {
		result.whiteBear = bearResult;
		result.whiteCircle = circleResult;
		break;
	}
	case color::green: {
		result.greenBear = bearResult;
		result.greenCircle = circleResult;
		result.greenWhiteSnake = snakeResult;
		break;
	}
	case color::orange: {
		result.orangeBear = bearResult;
		result.orangeCircle = circleResult;
		result.orangeBlackSnake = snakeResult;
		break;
	}
	case color::lightRed: {
		result.lightRedBear = bearResult;
		result.lightRedCircle = circleResult;
		break;
	}
	case color::darkRed: {
		result.darkRedBear = bearResult;
		result.darkRedCircle = circleResult;
		break;
	}
	case color::yellow: {
		result.yellowBear = bearResult;
		result.yellowCircle = circleResult;
		result.yellowRedSnake = snakeResult;
		break;
	}
	}

	std::cout << circleResult << " circles,\t"
		<< bearResult << " bears\t"
		<< snakeResult << " snakes\t"
		<< colorNames[colorToDetect] << std::endl;

#if  DISPLAY == 2
	cv::imshow("srcMaskedCircles " + colorNames[colorToDetect], srcMaskedCircles);
	cv::imshow("maskCircles out " + colorNames[colorToDetect], maskCircles);

	cv::imshow("srcMaskedBears " + colorNames[colorToDetect], srcMaskedBears);
	cv::imshow("maskBears out " + colorNames[colorToDetect], maskBears);

	cv::imshow("totalMask " + colorNames[colorToDetect], totalMask);
	cv::imshow("outputImg out " + colorNames[colorToDetect], srcMaskedCirclesBears);

#endif
	return srcMaskedCirclesBears;
}

std::vector<oneImgResult> jellyRecognizer::detectJellysAllImgs()
{
	std::vector<oneImgResult> localResult;
	for (int j = 0; j < inputPictures.size(); j++) {
		std::cout << "\nIMAGE: " << fileNames[j] << std::endl;
		oneImgResult imgResult;

		for (int i = 0; i < COLOR_NUMBER; i++) {
			detectJellysOneImg(inputPictures[j], (color)i, imgResult);
		}

		results.push_back(imgResult);
		localResult.push_back(imgResult);

#if DISPLAY >= 1
		cv::imshow("src", editedImg);
		waitKey(0);
#endif

	}
	return std::vector<oneImgResult>();
}

detectionParamsSet jellyRecognizer::prepareDetectionParamsSet(color colorToDetect)
{
	detectionParamsSet params;
	switch (colorToDetect) {
	case color::white: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = color::white;
		circleFirst.medianBlur = 9;
		circleFirst.colorHigh = cv::Scalar(WHITE_CIRCLE_FIRST_HIGH);
		circleFirst.colorLow = cv::Scalar(WHITE_CIRCLE_FIRST_LOW);
		circleFirst.errosionKernelSize = 3;
		circleFirst.closingKernelSize = 10;
		circleFirst.gaussianBlur = 9;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 30;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 10;
		circleFirst.houghCircleParams[5] = 35;
		params.cirlesDetection.push_back(circleFirst);

		circleMaskParam circleSecond;
		circleSecond.paramsColor = color::white;
		circleSecond.medianBlur = 9;
		circleSecond.colorHigh = cv::Scalar(WHITE_CIRCLE_SECOND_HIGH);
		circleSecond.colorLow = cv::Scalar(WHITE_CIRCLE_SECOND_LOW);
		circleSecond.errosionKernelSize = 3;
		circleSecond.closingKernelSize = 10;
		circleSecond.gaussianBlur = 9;
		circleSecond.houghCircleParams[0] = 1;
		circleSecond.houghCircleParams[1] = 40;
		circleSecond.houghCircleParams[2] = 75;
		circleSecond.houghCircleParams[3] = 20;
		circleSecond.houghCircleParams[4] = 10;
		circleSecond.houghCircleParams[5] = 35;
		params.cirlesDetection.push_back(circleSecond);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = color::white;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(WHITE_BEAR_HIGH);
		bearFirst.colorLow = cv::Scalar(WHITE_BEAR_LOW);
		bearFirst.closingKernelSize = 10;
		bearFirst.cutSize.low = 150;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = -1;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);
		break;
	}
	case color::green: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = color::green;
		circleFirst.medianBlur = 3;
		circleFirst.colorHigh = cv::Scalar(GREEN_HIGH);
		circleFirst.colorLow = cv::Scalar(GREEN_LOW);
		circleFirst.errosionKernelSize = 3;
		circleFirst.closingKernelSize = 5;
		circleFirst.gaussianBlur = 5;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 30;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 7;
		circleFirst.houghCircleParams[5] = 20;
		params.cirlesDetection.push_back(circleFirst);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = color::green;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(GREEN_HIGH);
		bearFirst.colorLow = cv::Scalar(GREEN_LOW);
		bearFirst.closingKernelSize = 6;
		bearFirst.cutSize.low = 150;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = 80;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);

		blobMaskParam snakeFirst;
		snakeFirst.paramsColor = color::green;
		snakeFirst.medianBlur = 9;
		snakeFirst.colorHigh = cv::Scalar(GREEN_HIGH);
		snakeFirst.colorLow = cv::Scalar(GREEN_LOW);
		snakeFirst.closingKernelSize = 6;
		snakeFirst.cutSize.low = 800;
		snakeFirst.cutSize.high = -1;
		snakeFirst.cutRatio.low = 0;
		snakeFirst.cutRatio.high = 40;
		snakeFirst.cutPerc.low = 1;
		snakeFirst.cutPerc.high = -1;
		params.snakeDetection.push_back(snakeFirst);
		break;
	}
	case color::orange: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = color::orange;
		circleFirst.medianBlur = 3;
		circleFirst.colorHigh = cv::Scalar(ORANGE_HIGH);
		circleFirst.colorLow = cv::Scalar(ORANGE_LOW);
		circleFirst.errosionKernelSize = 3;
		circleFirst.closingKernelSize = 5;
		circleFirst.gaussianBlur = 5;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 30;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 7;
		circleFirst.houghCircleParams[5] = 25;
		params.cirlesDetection.push_back(circleFirst);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = color::orange;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(ORANGE_HIGH);
		bearFirst.colorLow = cv::Scalar(ORANGE_LOW);
		bearFirst.closingKernelSize = 6;
		bearFirst.cutSize.low = 150;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = 80;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);

		blobMaskParam snakeFirst;
		snakeFirst.paramsColor = color::orange;
		snakeFirst.medianBlur = 9;
		snakeFirst.colorHigh = cv::Scalar(ORANGE_HIGH);
		snakeFirst.colorLow = cv::Scalar(ORANGE_LOW);
		snakeFirst.closingKernelSize = 6;
		snakeFirst.cutSize.low = 800;
		snakeFirst.cutSize.high = -1;
		snakeFirst.cutRatio.low = 0;
		snakeFirst.cutRatio.high = 40;
		snakeFirst.cutPerc.low = 1;
		snakeFirst.cutPerc.high = -1;
		params.snakeDetection.push_back(snakeFirst);
		break;

	}
	case color::lightRed: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = lightRed;
		circleFirst.medianBlur = 3;
		circleFirst.colorHigh = cv::Scalar(LIGHT_RED_HIGH);
		circleFirst.colorLow = cv::Scalar(LIGHT_RED_LOW);
		circleFirst.errosionKernelSize = 0;
		circleFirst.closingKernelSize = 12;
		circleFirst.closingShape = cv::MORPH_RECT;
		circleFirst.gaussianBlur = 5;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 10;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 7;
		circleFirst.houghCircleParams[5] = 25;
		params.cirlesDetection.push_back(circleFirst);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = lightRed;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(LIGHT_RED_HIGH);
		bearFirst.colorLow = cv::Scalar(LIGHT_RED_LOW);
		bearFirst.closingKernelSize = 6;
		bearFirst.cutSize.low = 150;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = 80;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);
		break;
	}
	case color::darkRed: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = darkRed;
		circleFirst.medianBlur = 3;
		circleFirst.colorHigh = cv::Scalar(DARK_RED_HIGH);
		circleFirst.colorLow = cv::Scalar(DARK_RED_LOW);
		circleFirst.errosionKernelSize = 3;
		circleFirst.closingKernelSize = 8;
		circleFirst.closingShape = cv::MORPH_RECT;
		circleFirst.gaussianBlur = 5;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 10;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 7;
		circleFirst.houghCircleParams[5] = 25;
		params.cirlesDetection.push_back(circleFirst);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = darkRed;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(DARK_RED_HIGH);
		bearFirst.colorLow = cv::Scalar(DARK_RED_LOW);
		bearFirst.closingKernelSize = 2;
		bearFirst.cutSize.low = 330;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = 80;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);
		break;
	}
	case color::yellow: {
		circleMaskParam circleFirst;
		circleFirst.paramsColor = yellow;
		circleFirst.medianBlur = 3;
		circleFirst.colorHigh = cv::Scalar(YELLOW_HIGH);
		circleFirst.colorLow = cv::Scalar(YELLOW_LOW);
		circleFirst.errosionKernelSize = 3;
		circleFirst.closingKernelSize = 8;
		circleFirst.closingShape = cv::MORPH_RECT;
		circleFirst.gaussianBlur = 5;
		circleFirst.houghCircleParams[0] = 1;
		circleFirst.houghCircleParams[1] = 10;
		circleFirst.houghCircleParams[2] = 75;
		circleFirst.houghCircleParams[3] = 20;
		circleFirst.houghCircleParams[4] = 7;
		circleFirst.houghCircleParams[5] = 25;
		params.cirlesDetection.push_back(circleFirst);

		blobMaskParam bearFirst;
		bearFirst.paramsColor = yellow;
		bearFirst.medianBlur = 9;
		bearFirst.colorHigh = cv::Scalar(YELLOW_HIGH);
		bearFirst.colorLow = cv::Scalar(YELLOW_LOW);
		bearFirst.closingKernelSize = 2;
		bearFirst.cutSize.low = 330;
		bearFirst.cutSize.high = -1;
		bearFirst.cutRatio.low = 40;
		bearFirst.cutRatio.high = 80;
		bearFirst.cutPerc.low = 60;
		bearFirst.cutPerc.high = -1;
		params.bearDetection.push_back(bearFirst);

		blobMaskParam snakeFirst;
		snakeFirst.paramsColor = color::yellow;
		snakeFirst.medianBlur = 9;
		snakeFirst.colorHigh = cv::Scalar(YELLOW_HIGH);
		snakeFirst.colorLow = cv::Scalar(YELLOW_LOW);
		snakeFirst.closingKernelSize = 6;
		snakeFirst.cutSize.low = 800;
		snakeFirst.cutSize.high = -1;
		snakeFirst.cutRatio.low = 0;
		snakeFirst.cutRatio.high = 40;
		snakeFirst.cutPerc.low = 1;
		snakeFirst.cutPerc.high = -1;
		params.snakeDetection.push_back(snakeFirst);
		break;
	}
	}
	return params;
}

std::tuple<cv::Mat, int> jellyRecognizer::detectCircles(cv::Mat scrNotBlurred, std::vector<circleMaskParam> cirlesDetectionParams, bool positiveMask) {

	cv::Mat mask;
	mask.create(scrNotBlurred.rows, scrNotBlurred.cols, CV_8UC1);
	if (positiveMask) {
		mask.setTo(0);
	}
	else {
		mask.setTo(255);
	}
	for (int round = 0; round < cirlesDetectionParams.size(); round++) {
		cv::Mat roundMask;
		std::tuple<cv::Mat, int> roundTuple;
		roundTuple = makeCircleMaskOneRound(scrNotBlurred, cirlesDetectionParams[round], positiveMask);
		roundMask = std::get<0>(roundTuple);
		cv::Mat tempMask;
		mask.copyTo(tempMask, roundMask);
		mask = tempMask;
	}

	cv::Mat maskBlurred;
	cv::GaussianBlur(mask, maskBlurred, cv::Size(cirlesDetectionParams[0].gaussianBlur, cirlesDetectionParams[0].gaussianBlur), 0);
	std::vector<cv::Vec3f> circles;
	cv::HoughCircles(maskBlurred, circles, CV_HOUGH_GRADIENT,
		cirlesDetectionParams[0].houghCircleParams[0], cirlesDetectionParams[0].houghCircleParams[1], cirlesDetectionParams[0].houghCircleParams[2],
		cirlesDetectionParams[0].houghCircleParams[3], cirlesDetectionParams[0].houghCircleParams[4], cirlesDetectionParams[0].houghCircleParams[5]);
	int circleCounter = 0;
	circleCounter = circles.size();
	std::tuple<cv::Mat, int> returnTuple;
	returnTuple = std::make_tuple(mask, circleCounter);

	return returnTuple;
}
std::tuple<cv::Mat, int> jellyRecognizer::makeCircleMaskOneRound(cv::Mat scrNotBlurred, circleMaskParam params, bool positiveMask)
{
	std::vector<cv::Vec3f> circles;
	std::tuple<cv::Mat, int> returnTuple;
	returnTuple = this->makeCircleMaskOneRound(scrNotBlurred, params, circles, positiveMask);
	return returnTuple;
}
std::tuple<cv::Mat, int> jellyRecognizer::makeCircleMaskOneRound(cv::Mat scrNotBlurred, circleMaskParam params, std::vector<cv::Vec3f>& circles, bool positiveMask)
{
	bool displayAdditional = false;
	cv::Mat srcBlurred, srcBlurredHSV, colorMask, colorMaskEddited;
	cv::medianBlur(scrNotBlurred, srcBlurred, params.medianBlur);

	cv::cvtColor(srcBlurred, srcBlurredHSV, cv::COLOR_BGR2HSV);
	if (params.colorLow.val[0] < 0) {
		cv::Mat range0mask; // mask 0 - ?
		cv::Mat range1mask; // mask ? - 180
		cv::Scalar range0low(0, params.colorLow.val[1], params.colorLow.val[2]);
		cv::Scalar range0high(params.colorHigh);
		cv::Scalar range1low(180 + params.colorLow.val[0], params.colorLow.val[1], params.colorLow.val[2]);
		cv::Scalar range1high(180, params.colorHigh.val[1], params.colorHigh.val[2]);

		inRange(srcBlurredHSV, range0low, range0high, range0mask);
		inRange(srcBlurredHSV, range1low, range1high, range1mask);

		colorMask = cv::max(range0mask, range1mask); // combining the two thresholds

		cv::imshow("range0mask", range0mask);
		cv::imshow("range1mask", range1mask);
		cv::imshow("colorMask", colorMask);

	}
	else {
		inRange(srcBlurredHSV, params.colorLow, params.colorHigh, colorMask);
	}

	if (displayAdditional) cv::imshow("colorMask", colorMask);
	colorMaskEddited = colorMask.clone();
	if (params.errosionKernelSize > 0)	cv::morphologyEx(colorMaskEddited, colorMaskEddited, cv::MORPH_ERODE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(params.errosionKernelSize, params.errosionKernelSize)));
	if (params.closingKernelSize > 0) cv::morphologyEx(colorMaskEddited, colorMaskEddited, cv::MORPH_CLOSE, cv::getStructuringElement(params.closingShape, cv::Size(params.closingKernelSize, params.closingKernelSize)));
	cv::GaussianBlur(colorMaskEddited, colorMaskEddited, cv::Size(params.gaussianBlur, params.gaussianBlur), 0);

	if (displayAdditional)  cv::imshow("colorMaskEddited", colorMaskEddited);

	cv::Mat circleMask;
	circleMask.create(srcBlurred.size(), CV_8UC1);
	circleMask.setTo(0);

	HoughCircles(colorMaskEddited, circles, CV_HOUGH_GRADIENT,
		params.houghCircleParams[0], params.houghCircleParams[1], params.houghCircleParams[2], params.houghCircleParams[3], params.houghCircleParams[4], params.houghCircleParams[5]);

	int circlesCounter = 0;
	for (size_t i = 0; i < circles.size(); i++) {
		cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);
		if (radius > 0) {
			cv::circle(circleMask, center, 1.3 * radius, 255, cv::FILLED, 8, 0);
			++circlesCounter;
		}
	}
	if (!positiveMask) {
		circleMask = 255 - circleMask;
	}

	std::tuple<cv::Mat, int> returnTuple;
	returnTuple = std::make_tuple(circleMask, circlesCounter);
	return returnTuple;
}

std::tuple<cv::Mat, int, int> jellyRecognizer::detectShapes(cv::Mat srcMasked, std::vector<blobMaskParam> bearDetectionParams, bool positiveMask)
{
	cv::Mat mask;
	mask.create(srcMasked.rows, srcMasked.cols, CV_8UC1);
	if (positiveMask) {
		mask.setTo(0);
	}
	else {
		mask.setTo(255);
	}

	int additionalCircles = 0;
	if (bearDetectionParams.size() == 0) {
		std::tuple<cv::Mat, int, int> totalTuple, returnTuple;
		returnTuple = std::make_tuple(mask, 0, 0);
		return returnTuple;
	}
	for (int round = 0; round < bearDetectionParams.size(); round++) {
		cv::Mat roundMask;
		std::tuple<cv::Mat, int, int> roundTuple;
		roundTuple = detectShapesOneRound(srcMasked, bearDetectionParams[round], positiveMask);
		roundMask = std::get<0>(roundTuple);
		additionalCircles += std::get<2>(roundTuple);
		cv::Mat tempMask;
		mask.copyTo(tempMask, roundMask);
		mask = tempMask;
	}

	std::tuple<cv::Mat, int, int> totalTuple, returnTuple;
	cv::Mat outputMask;
	int outbutBearCounter;
	if (positiveMask) {
		totalTuple = detectShapesRectangles(mask, bearDetectionParams[0].cutSize, bearDetectionParams[0].cutRatio, bearDetectionParams[0].cutPerc);
		outputMask = std::get<0>(totalTuple);
	}
	else {
		totalTuple = detectShapesRectangles(255 - mask, bearDetectionParams[0].cutSize, bearDetectionParams[0].cutRatio, bearDetectionParams[0].cutPerc);
		outputMask = 255 - std::get<0>(totalTuple);
	}
	outbutBearCounter = std::get<1>(totalTuple);
	returnTuple = std::make_tuple(outputMask, outbutBearCounter, additionalCircles);

	return returnTuple;
}
std::tuple<cv::Mat, int, int> jellyRecognizer::detectShapesOneRound(cv::Mat srcMasked, blobMaskParam param, bool positiveMask) {
	cv::Mat srcBlurred, srcBlurredHSV, colorMask, colorMaskClosed;
	cv::medianBlur(srcMasked, srcBlurred, param.medianBlur);



	cv::cvtColor(srcBlurred, srcBlurredHSV, cv::COLOR_BGR2HSV);
	inRange(srcBlurredHSV, param.colorLow, param.colorHigh, colorMask);

	cv::morphologyEx(colorMask, colorMaskClosed, cv::MORPH_CLOSE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(param.closingKernelSize, param.closingKernelSize)));
#if DISPLAY >= 3
	cv::imshow("srcBlurredBear", srcBlurred);
	cv::imshow("colorMask", colorMask);
	cv::imshow("colorMaskClosed", colorMaskClosed);
#endif

	std::tuple<cv::Mat, int, int> returnTuple;
	returnTuple = detectShapesRectangles(colorMaskClosed, param.cutSize, param.cutRatio, param.cutPerc);
	cv::Mat outputMask, shapeMask;
	shapeMask = std::get<0>(returnTuple);
	if (!positiveMask) {
		shapeMask = 255 - shapeMask;
	}
	srcMasked.copyTo(outputMask, shapeMask);
	return returnTuple;
}
std::tuple<cv::Mat, int, int> jellyRecognizer::detectShapesRectangles(cv::Mat srcMaskedThresholded, limits cutSize, limits cutRatio, limits cutPerc)
{
	if (cutSize.low <= 0) cutSize.low = 0;
	if (cutSize.high <= 0) cutSize.high = srcMaskedThresholded.rows * srcMaskedThresholded.cols;
	if (cutRatio.low <= 0) cutRatio.low = 0;
	if (cutRatio.high <= 0) cutRatio.high = 100;
	if (cutPerc.low <= 0) cutPerc.low = 0;
	if (cutPerc.high <= 0) cutPerc.high = 100;

	int additionalCircles = 0;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::Mat contourMat = srcMaskedThresholded.clone();
	findContours(contourMat, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	std::vector<cv::RotatedRect> minRect(contours.size());
	std::vector<cv::RotatedRect> minEllipse(contours.size());

	for (int i = 0; i < contours.size(); i++)
	{
		minRect[i] = cv::minAreaRect(cv::Mat(contours[i]));
		if (contours[i].size() > 5)
		{
			minEllipse[i] = cv::fitEllipse(cv::Mat(contours[i]));
		}
	}
	cv::RNG rng(12345);

	cv::Mat mask, drawing;
	mask.create(srcMaskedThresholded.rows, srcMaskedThresholded.cols, CV_8UC1);
	mask.setTo(0);
	drawing = srcMaskedThresholded.clone();
	int shapesCounter = 0;
	for (int i = 0; i < contours.size(); i++)
	{
		if (!(cv::contourArea(contours[i]) > 0)) {
			continue;
		}
		// rotated rectangle
		cv::Point2f rect_points[4]; minRect[i].points(rect_points);
		std::vector<int> pointsX, pointsY;
		int rotatedRatio, rotatedPerc;
		rotatedRatio = std::min<double>(minRect[i].size.width, minRect[i].size.height) * 100 / std::max<double>(minRect[i].size.width, minRect[i].size.height);
		rotatedPerc = cv::contourArea(contours[i]) * 100 / minRect[i].size.area();

		if (minRect[i].size.area() < cutSize.low || minRect[i].size.area() > cutSize.high ||
			rotatedRatio < cutRatio.low || rotatedRatio > cutRatio.high ||
			rotatedPerc < cutPerc.low || rotatedPerc > cutPerc.high) {
			cv::Point rook_points[1][20];
			rook_points[0][0] = rect_points[0];
			rook_points[0][1] = rect_points[1];
			rook_points[0][2] = rect_points[2];
			rook_points[0][3] = rect_points[3];
			const cv::Point* ppt[1] = { rook_points[0] };

			int npt[] = { 4 };

#if VERBOSE >= 3
			std::cout << "rejected shape: c=" << cv::contourArea(contours[i]) << "\ts=" << minRect[i].size.area() << "\tr=" << rotatedRatio << "\tp=" << rotatedPerc << std::endl;
#endif
		}
		else {
			++shapesCounter;
			drawContours(mask, contours, i, 255, CV_FILLED, 1, std::vector<cv::Vec4i>(), 0, cv::Point());
#if VERBOSE >= 2
			std::cout << "detected shape: c=" << cv::contourArea(contours[i]) << "\ts=" << minRect[i].size.area() << "\tr=" << rotatedRatio << "\tp=" << rotatedPerc << std::endl;
#endif
		}
		if (minRect[i].size.area() > cutSize.low && minRect[i].size.area() < cutSize.high&&
			rotatedRatio > cutRatio.high && rotatedPerc > cutPerc.low) {
			++additionalCircles;
		}
	}
	std::tuple<cv::Mat, int, int> returnTuple;
	returnTuple = std::make_tuple(mask, shapesCounter, additionalCircles);
	return returnTuple;
}
